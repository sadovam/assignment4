package com.shpp.p2p.cs.asadov.assignment4;

import acm.graphics.GObject;
import acm.graphics.GOval;
import acm.graphics.GRect;
import acm.util.RandomGenerator;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * This program draw pyramid of bricks
 * in the bottom of the window.
 * <p>
 * You can change width and height of application window
 * and parameters of bricks and see that happened.
 */
public class Assignment4Part1 extends WindowProgram {
    /**
     * Width and height of application window in pixels
     */
    public static final int APPLICATION_WIDTH = 400;
    public static final int APPLICATION_HEIGHT = 600;

    /**
     * Dimensions of game board (usually the same)
     */
    private static final int WIDTH = APPLICATION_WIDTH;
    private static final int HEIGHT = APPLICATION_HEIGHT;

    /**
     * Dimensions of the paddle
     */
    private static final int PADDLE_WIDTH = 60;
    private static final int PADDLE_HEIGHT = 10;

    /**
     * Offset of the paddle up from the bottom
     */
    private static final int PADDLE_Y_OFFSET = 30;

    /**
     * Number of bricks per row
     */
    private static final int NBRICKS_PER_ROW = 10;

    /**
     * Number of rows of bricks
     */
    private static final int NBRICK_ROWS = 10;

    /**
     * Separation between bricks
     */
    private static final int BRICK_SEP = 4;

    /**
     * Width of a brick
     */
    private static final int BRICK_WIDTH =
            (WIDTH - (NBRICKS_PER_ROW - 1) * BRICK_SEP) / NBRICKS_PER_ROW;

    /**
     * Height of a brick
     */
    private static final int BRICK_HEIGHT = 8;

    /**
     * Radius of the ball in pixels
     */
    private static final int BALL_RADIUS = 10;

    /**
     * Offset of the top brick row from the top
     */
    private static final int BRICK_Y_OFFSET = 70;

    /**
     * Number of turns
     */
    private static final int NTURNS = 7;

    private GRect paddle;
    private GOval ball;
    private double widthMinusPaddle;
    private double paddleY;
    private double vx, vy;
    RandomGenerator rgen = RandomGenerator.getInstance();
    Color[] colors = {Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.CYAN};
    boolean ballOnPaddle = false;
    int bricksCounter = NBRICK_ROWS * NBRICKS_PER_ROW;

    public void run() {
        initGame();
        addMouseListeners();
        startGame();
    }

    private void initGame() {
        widthMinusPaddle = getWidth() - PADDLE_WIDTH;
        paddleY = getHeight() - PADDLE_Y_OFFSET;
        createBricks();
        drawPaddle();
        drawBall();
        initPaddle();
        initBall();
        waitForClick();
        ballOnPaddle = false;


    }

    private void startGame() {
        int turns = NTURNS;

        while(turns > 0 && bricksCounter > 0) {
            if (!ballMoves()) {

                turns--;

                if (turns != 0) {
                    initPaddle();
                    initBall();
                    waitForClick();
                    ballOnPaddle = false;
                }


            }
            pause(10);
        }
        remove(ball);
        if(bricksCounter <= 0) {
          setBackground(Color.GREEN);
        } else {
            setBackground(Color.RED);
        }


    }

    public void mouseMoved(MouseEvent mouseEvent) {
        movePaddleTo(mouseEvent.getX());
    }

    private void movePaddleTo(double x) {
        // x - center of paddle
        double startX = x - PADDLE_WIDTH / 2.0;
        if (startX < 0)
            startX = 0;
        if (startX > widthMinusPaddle)
            startX = widthMinusPaddle;

        paddle.setLocation(startX, paddleY);
        if (ballOnPaddle) {
            ball.setLocation(startX + PADDLE_WIDTH / 2.0 - BALL_RADIUS, paddleY - BALL_RADIUS * 2);
        }
    }

    private void createBricks() {
        double startX = (getWidth() - NBRICKS_PER_ROW * (BRICK_SEP + BRICK_WIDTH) + BRICK_SEP) / 2.0;
        for(int i = 0; i < NBRICK_ROWS; i++) {
            for (int j = 0; j < NBRICKS_PER_ROW; j++) {
                drawBrick(startX + j * (BRICK_WIDTH + BRICK_SEP),
                        BRICK_Y_OFFSET + i * (BRICK_HEIGHT + BRICK_SEP),
                        colors[i / 2]);
            }
        }
    }





    private void initPaddle() {
        paddle.setLocation(widthMinusPaddle / 2.0,
                paddleY);
    }

    private void drawPaddle() {
        paddle = new GRect(widthMinusPaddle / 2.0,
                paddleY,
                PADDLE_WIDTH, PADDLE_HEIGHT);
        paddle.setFilled(true);
        add(paddle);
    }

    private void drawBall() {
        ball = new GOval(0,
                0,
                BALL_RADIUS * 2.0, BALL_RADIUS * 2.0);
        ball.setFilled(true);
        add(ball);
    }

    private void drawBrick(double x, double y, Color color) {
        GRect brick = new GRect(x, y, BRICK_WIDTH, BRICK_HEIGHT);
        brick.setFilled(true);
        brick.setColor(color);
        add(brick);
    }

    private void initBall() {
        ball.setLocation(getWidth() / 2.0 - BALL_RADIUS,paddleY - BALL_RADIUS * 2);
        vx = rgen.nextDouble(1.0, 3.0);
        if (rgen.nextBoolean(0.5))
            vx = -vx;
        vy = -3.0;
        ballOnPaddle = true;

    }

    private boolean ballMoves() {
        ball.move(vx, vy);

        if (ball.getX() <= 0 || ball.getX() >= getWidth() - BALL_RADIUS * 2)
            vx = -vx;
        if (ball.getY() <= 0)
            vy = -vy;
        if (ball.getY() >= paddleY - BALL_RADIUS * 2 + 1)
            return false;
        GObject obj = getCollidingObject();
        if (obj != null)
            vy = -vy;
        if (obj != null && obj != paddle) {
            remove(obj);
            bricksCounter--;

        }
        return true;
    }

    private GObject getCollidingObject() {
        double x = ball.getX();
        double y = ball.getY();
        GObject obj;
        for (int i = 0; i < 4; i++) {
            obj = getElementAt(x + (i % 2) * BALL_RADIUS * 2, y + (i / 2) * BALL_RADIUS * 2);
            if (obj != null)
                return obj;
        }
        return null;
    }
}