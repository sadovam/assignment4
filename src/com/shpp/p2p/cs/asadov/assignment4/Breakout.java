package com.shpp.p2p.cs.asadov.assignment4;

import acm.graphics.GLabel;
import acm.graphics.GObject;
import acm.graphics.GOval;
import acm.graphics.GRect;
import acm.util.RandomGenerator;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * This program implements The Breakout Game.
 */
public class Breakout extends WindowProgram {
    /**
     * Width and height of application window in pixels
     */
    public static final int APPLICATION_WIDTH = 400;
    public static final int APPLICATION_HEIGHT = 600;

    /**
     * Dimensions of game board (usually the same)
     */
    private static final int WIDTH = APPLICATION_WIDTH;
    private static final int HEIGHT = APPLICATION_HEIGHT;

    /**
     * Dimensions of the paddle
     */
    private static final int PADDLE_WIDTH = 60;
    private static final int PADDLE_HEIGHT = 10;

    /**
     * Offset of the paddle up from the bottom
     */
    private static final int PADDLE_Y_OFFSET = 30;

    /**
     * Number of bricks per row
     */
    private static final int NBRICKS_PER_ROW = 10;

    /**
     * Number of rows of bricks
     */
    private static final int NBRICK_ROWS = 10;

    /**
     * Separation between bricks
     */
    private static final int BRICK_SEP = 4;

    /**
     * Width of a brick
     */
    private static final int BRICK_WIDTH =
            (WIDTH - (NBRICKS_PER_ROW - 1) * BRICK_SEP) / NBRICKS_PER_ROW;

    /**
     * Height of a brick
     */
    private static final int BRICK_HEIGHT = 8;

    /**
     * Radius of the ball in pixels
     */
    private static final int BALL_RADIUS = 10;

    /**
     * Offset of the top brick row from the top
     */
    private static final int BRICK_Y_OFFSET = 70;

    /**
     * Number of turns
     */
    private static final int NTURNS = 13;

    /**
     * Speed of Y and max speed of X
     */
    private static final double SPEED = 3.9;

    /**
     * Pause duration between frames
     */
    private static final int PAUSE = 13;

    /**
     * Colors of bricks
     */
    private static final Color[] COLORS = {Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.CYAN};

    // Paddle object
    private GRect paddle;
    // Ball object
    private GOval ball;
    // Random generator
    RandomGenerator rgen = RandomGenerator.getInstance();
    // X and Y velocity
    private double vx, vy;
    // If ball situated on paddle (before starting the turn)
    boolean ballOnPaddle;
    // Counter of bricks (number of bricks left)
    int bricksCounter;

    public void run() {
        addMouseListeners();
        playing();
    }

    /**
     * Implements breakout game in infinity loop
     */
    private void playing() {
        while (true) {
            initGame();
            playGame();
        }
    }

    /**
     * Initialises new game: clear screen, create new bricks, init random generator
     */
    private void initGame() {
        removeAll();
        setBackground(Color.WHITE);
        initBricks();
        bricksCounter = NBRICK_ROWS * NBRICKS_PER_ROW;
    }

    /**
     * Initialises and plays NTURN turns
     */
    private void playGame() {
        for (int i = 0; i < NTURNS; i++) {
            // If playTurn return true, player win...
            if (playTurn()) {
                youWin();
                return;
            }
            // ...else ball lost and turn ends
            youLostBall(NTURNS - i - 1);
        }
        // If player not wins in NTURN turns, he loses
        youLoose();
    }

    /**
     * Initialisations of new turn
     */
    private void initTurn() {
        initPaddle();
        initBall();
        initSpeed();
        ballOnPaddle = true; // Ball moves with paddle
        waitForClick();
        ballOnPaddle = false; // Ball not moves with paddle
    }

    /**
     * Play one turn of game (ball moves in cicle)
     *
     * @return true if all bricks destroyed or false if ball lost
     */
    public boolean playTurn() {
        initTurn();
        while (true) {
            if (!ballMoves())
                return false;
            if (bricksCounter == 0)
                return true;
            pause(PAUSE);
        }
    }

    /**
     * Implements one move of ball
     *
     * @return false if ball is lost or true otherwise
     */
    private boolean ballMoves() {
        // x, y - coordinates of the center of ball there it may be in next step
        double x = ball.getX() + BALL_RADIUS + vx, y = ball.getY() + BALL_RADIUS + vy;

        // if strike bottom
        if (y >= getHeight() - BALL_RADIUS)
            return false;

        // Test strike top and bottom points of ball
        if ((y - BALL_RADIUS) < 0 ||                            // if strike top by top of ball
                testObjectCollation(x, y - BALL_RADIUS) ||   // if strike brick by top of ball
                testObjectCollation(x, y + BALL_RADIUS))     // if strike brick or paddle by bottom of ball
            vy = -vy;                                           // mirror y velocity

        // Test strike left and right points of ball...
        if ((x - BALL_RADIUS) < 0 ||                            // if strike left side by left of ball
                (x + BALL_RADIUS) >= getWidth())                // if strike right side by right of ball
            vx = -vx;                                           // mirror x velocity
        // ...and corner strike
        if (testObjectCollation(x - BALL_RADIUS, y) ||       // if strike paddle or brick by left of ball
                testObjectCollation(x + BALL_RADIUS, y)) {   // if strike paddle or brick by right of ball
            vx = -vx;                                           // mirror x velocity and
            vy = -vy;                                           // mirror y velocity
        }
        ball.move(vx, vy);                                      // move ball with new velocity parameters
        return true;
    }

    /**
     * Tests if any object in coordinates x, y
     * and if find brick it destroys and brick counter decrements
     *
     * @param x - x coordinates to seek for object
     * @param y - y coordinates to seek for object
     * @return true if find paddle or brick object or false otherwise
     */
    private boolean testObjectCollation(double x, double y) {
        GObject obj = getElementAt(x, y);
        if (obj != null && obj != ball) {
            if (obj != paddle) {
                bricksCounter--;
                remove(obj);
            }
            return true;
        }
        return false;
    }

    /**
     * Show message if player win
     */
    private void youWin() {
        setBackground(Color.GREEN);
        showMessage("You win :)");
        pause(500);
    }

    /**
     * Show message if player loose
     */
    private void youLoose() {
        setBackground(Color.RED);
        showMessage("You lose :(");
        pause(500);
    }

    /**
     * Show message if player lost ball
     */
    private void youLostBall(int turnsLeft) {
        ball.setVisible(false);
        paddle.setVisible(false);
        setBackground(Color.DARK_GRAY);
        GLabel label = showMessage("Balls left: " + turnsLeft);
        pause(500);
        setBackground(Color.WHITE);
        remove(label);
    }

    /**
     * Show message in the center of the window
     * @param message - message to show
     * @return label object
     */
    private GLabel showMessage(String message) {
        GLabel label = new GLabel(message);
        label.setFont("Serif-25");
        label.setColor(Color.WHITE);
        double x = (getWidth() - label.getWidth()) / 2;
        double y = (getHeight() + label.getAscent() / 2) / 2;
        add(label, x, y);
        return label;
    }

    /**
     * Bind function to mouse event
     * @param mouseEvent - event to bind function
     */
    public void mouseMoved(MouseEvent mouseEvent) {
        movePaddleTo(mouseEvent.getX());
    }

    /**
     * Move paddle to x coordinate
     * @param x - x coordinate of the center of paddle
     */
    private void movePaddleTo(double x) {
        double startX = x - PADDLE_WIDTH / 2.0;
        if (startX < 0)
            startX = 0;
        if (startX > getWidth() - PADDLE_WIDTH)
            startX = getWidth() - PADDLE_WIDTH;

        paddle.setLocation(startX, getHeight() - PADDLE_Y_OFFSET);
        // if ballOnPaddle they moves together
        if (ballOnPaddle) {
            ball.setLocation(startX + PADDLE_WIDTH / 2.0 - BALL_RADIUS,
                    getHeight() - PADDLE_Y_OFFSET - BALL_RADIUS * 2);
        }
    }


    /**
     * Creates wall of bricks
     */
    private void initBricks() {
        double startX = (getWidth() - NBRICKS_PER_ROW * (BRICK_SEP + BRICK_WIDTH) + BRICK_SEP) / 2.0;
        for (int i = 0; i < NBRICK_ROWS; i++) {
            for (int j = 0; j < NBRICKS_PER_ROW; j++) {
                drawBrick(startX + j * (BRICK_WIDTH + BRICK_SEP),
                        BRICK_Y_OFFSET + i * (BRICK_HEIGHT + BRICK_SEP),
                        COLORS[i / 2]);
            }
        }
    }


    /**
     * Create paddle if need and place it on start place
     */
    private void initPaddle() {
        if (paddle == null)
            drawPaddle();
        add(paddle);
        paddle.setLocation((getWidth() - PADDLE_WIDTH) / 2.0, getHeight() - PADDLE_Y_OFFSET);
        paddle.setVisible(true);
    }

    /**
     * Create ball if need and place it on start place
     */
    private void initBall() {
        if (ball == null)
            drawBall();
        add(ball);
        ball.setLocation(getWidth() / 2.0 - BALL_RADIUS, getHeight() - PADDLE_Y_OFFSET - BALL_RADIUS * 2);
        ball.setVisible(true);
    }

    /**
     * Initialises x and y velocity
     * x velocity is random
     */
    private void initSpeed() {
        vx = rgen.nextDouble(1.0, SPEED);
        if (rgen.nextBoolean(0.5))
            vx = -vx;
        vy = -SPEED;
    }

    /**
     * Draw paddle rect
     */
    private void drawPaddle() {
        paddle = new GRect(PADDLE_WIDTH, PADDLE_HEIGHT);
        paddle.setFilled(true);
    }

    /**
     * Draw ball oval
     */
    private void drawBall() {
        ball = new GOval(BALL_RADIUS * 2.0, BALL_RADIUS * 2.0);
        ball.setFilled(true);
    }

    /**
     * Draw brick rect
     */
    private void drawBrick(double x, double y, Color color) {
        GRect brick = new GRect(x, y, BRICK_WIDTH, BRICK_HEIGHT);
        brick.setFilled(true);
        brick.setColor(color);
        add(brick);
    }
}